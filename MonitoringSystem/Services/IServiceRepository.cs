﻿using MonitoringSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.Services
{
    public interface IServiceRepository
    {
        Task<bool> Set(ServiceViewModel service);
        Task<ServiceViewModel> Get(Guid id);
        Task<List<ServiceViewModel>> GetAll();
    }
}
