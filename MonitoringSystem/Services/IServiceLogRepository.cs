﻿using MonitoringSystem.DAL;
using MonitoringSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.Services
{
    public interface IServiceLogRepository
    {
        Task<bool> Set(ServiceLogViewModel serviceLog);
        Task<int> StatusesCount(Guid serviceId, Status status, DateFilterModel dateFilter);
    }
}
