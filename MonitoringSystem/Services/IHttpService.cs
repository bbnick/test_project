﻿using MonitoringSystem.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MonitoringSystem.Services
{
    public interface IHttpService
    {
        Task<Status> CheckService(string url);
    }
}
