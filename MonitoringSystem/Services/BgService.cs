﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MonitoringSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MonitoringSystem.Services
{
    public class BgService : IHostedService
    {
        private Timer _timer;
        private readonly BgServiceOptions _config;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public BgService(IOptions<BgServiceOptions> options, IServiceScopeFactory serviceScopeFactory)
        {
            _config = options.Value;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(CheckServices, null, TimeSpan.Zero, TimeSpan.FromSeconds(_config.UpdateTime));

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        private async void CheckServices(object state)
        {
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var httpService = scope.ServiceProvider.GetRequiredService<IHttpService>();
                var serviceRepository = scope.ServiceProvider.GetRequiredService<IServiceRepository>();
                var serviceLogRepository = scope.ServiceProvider.GetRequiredService<IServiceLogRepository>();

                var services = await serviceRepository.GetAll();

                foreach(var service in services)
                {
                    var status = await httpService.CheckService(service.Url);
                    var serviceLog = new ServiceLogViewModel
                    {
                        ServiceId = service.Id,
                        Status = status,
                        SurveyTime = DateTime.Now
                    };
                    await serviceLogRepository.Set(serviceLog);
                }
            }
        }
    }
}
