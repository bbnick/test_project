﻿using Microsoft.EntityFrameworkCore;
using MonitoringSystem.DAL;
using MonitoringSystem.Extensions;
using MonitoringSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.Services
{
    public class ServiceLogRepository : IServiceLogRepository
    {
        private readonly MSContext _dbContext;
        public ServiceLogRepository(MSContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> Set(ServiceLogViewModel serviceLog)
        {
            var serviceLogEntity = serviceLog.ToEntity();

            if (serviceLogEntity == null)
                return false;

            _dbContext.ServiceLogs.Add(serviceLogEntity);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<int> StatusesCount(Guid serviceId, Status status, DateFilterModel dateFilter)
        {
            return await _dbContext.ServiceLogs
                .ApplyDateFilter(dateFilter)
                .Where(x => x.ServiceId == serviceId)
                .Where(x => x.Status == status)
                .CountAsync();
        }
    }
}
