﻿using Microsoft.EntityFrameworkCore;
using MonitoringSystem.DAL;
using MonitoringSystem.Extensions;
using MonitoringSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.Services
{
    public class ServiceRepository : IServiceRepository
    {
        private readonly MSContext _dbContext;

        public ServiceRepository(MSContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ServiceViewModel> Get(Guid id)
        {
            try
            {
                var service = await _dbContext.Services.FindAsync(id);
                return service.ToViewModel();
            }
            catch
            {
                return null;
            }
        }

        public async Task<bool> Set(ServiceViewModel service)
        {
            var serviceEntity = service.ToEntity();

            if (serviceEntity == null)
                return false;

            _dbContext.Services.Add(serviceEntity);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<List<ServiceViewModel>> GetAll()
        {
            return await _dbContext.Services
                .Select(x => x.ToViewModel())
                .ToListAsync();
        }
    }
}
