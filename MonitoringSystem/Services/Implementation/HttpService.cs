﻿using MonitoringSystem.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MonitoringSystem.Services
{
    public class HttpService : IHttpService
    {
        readonly HttpClient _httpClient;

        public HttpService(IHttpClientFactory factory)
        {
            _httpClient = factory.CreateClient();
        }

        public async Task<Status> CheckService(string url)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, url);

            HttpResponseMessage responce;

            try
            {
                responce = await _httpClient.SendAsync(requestMessage);
            }
            catch
            {
                return Status.Unavailable;
            }

            if (responce.IsSuccessStatusCode)
                return Status.Available;
            else
                return Status.Unavailable;
        }
    }
}
