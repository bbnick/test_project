﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.DAL
{
    public class MSContext : DbContext
    {
        public DbSet<ServiceEntity> Services { get; set; }
        public DbSet<ServiceLogEntity> ServiceLogs { get; set; }

        public MSContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        public MSContext() { }
    }
}
