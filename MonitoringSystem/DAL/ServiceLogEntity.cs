﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MonitoringSystem.DAL
{
    public class ServiceLogEntity
    {
        public Guid Id { get; set; }

        public Guid ServiceId { get; set; }
        public virtual ServiceEntity Service { get; set; }

        public Status Status { get; set; }
        public DateTime SurveyTime { get; set; }

        public ServiceLogEntity()
        {
            Id = Guid.NewGuid();
        }
    }

    public enum Status
    {
        Available, Unavailable
    }
}
