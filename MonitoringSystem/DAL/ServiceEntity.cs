﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.DAL
{
    public class ServiceEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

        public virtual ICollection<ServiceLogEntity> ServiceLogs { get; set; }

        public ServiceEntity()
        {
            Id = Guid.NewGuid();
            ServiceLogs = new List<ServiceLogEntity>();
        }
    }
}
