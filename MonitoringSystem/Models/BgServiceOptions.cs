﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.Models
{
    public class BgServiceOptions
    {
        public float UpdateTime { get; set; }
    }
}
