﻿using MonitoringSystem.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace MonitoringSystem.Models
{
    public class ServiceLogViewModel
    {
        public Guid Id { get; set; }
        public Status Status { get; set; }
        public DateTime SurveyTime { get; set; }

        public Guid ServiceId { get; set; }

        public ServiceLogViewModel()
        {
            Id = Guid.NewGuid();
        }
    }
}
