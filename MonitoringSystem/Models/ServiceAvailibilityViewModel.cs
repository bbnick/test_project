﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.Models
{
    public class ServiceAvailibilityViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Availability { get; set; }
        public int FailureHourCount { get; set; }
        public int FailureDayCount { get; set; }
    }
}
