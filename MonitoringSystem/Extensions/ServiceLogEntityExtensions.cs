﻿using MonitoringSystem.DAL;
using MonitoringSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.Extensions
{
    public static class ServiceLogEntityExtensions
    {
        public static ServiceLogViewModel ToViewModel(this ServiceLogEntity slEntity)
        {
            if (slEntity == null)
                return null;

            return new ServiceLogViewModel
            {
                Id = slEntity.Id,
                Status = slEntity.Status,
                SurveyTime = slEntity.SurveyTime,
                ServiceId = slEntity.ServiceId
            };
        }

        public static ServiceLogEntity ToEntity(this ServiceLogViewModel serviceLog)
        {
            if (serviceLog == null)
                return null;

            return new ServiceLogEntity
            {
                Id = serviceLog.Id,
                Status = serviceLog.Status,
                SurveyTime = serviceLog.SurveyTime,
                ServiceId = serviceLog.ServiceId
            };
        }
    }
}
