﻿using MonitoringSystem.DAL;
using MonitoringSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.Extensions
{
    public static class QueryExtensions
    {
        public static IQueryable<ServiceLogEntity> ApplyDateFilter(this IQueryable<ServiceLogEntity> query, DateFilterModel dateFilter)
        {
            if (dateFilter == null)
                return query;

            return query.Where(x => x.SurveyTime >= dateFilter.DateFrom && x.SurveyTime < dateFilter.DateTo);
        }
    }
}
