﻿using MonitoringSystem.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.Extensions
{
    public static class StatusExtensions
    {
        public static string ToViewString(this Status status)
        {
            switch(status)
            {
                case Status.Available:
                    return "Доступен";
                case Status.Unavailable:
                    return "Не доступен";
                default:
                    return "";
            }
        }
    }
}
