﻿using MonitoringSystem.DAL;
using MonitoringSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringSystem.Extensions
{
    public static class ServiceEntityExtensions
    {
        public static ServiceViewModel ToViewModel(this ServiceEntity serviceEntity)
        {
            if (serviceEntity == null)
                return null;

            return new ServiceViewModel
            {
                Id = serviceEntity.Id,
                Name = serviceEntity.Name,
                Url = serviceEntity.Url
            };
        }

        public static ServiceEntity ToEntity(this ServiceViewModel service)
        {
            if (service == null)
                return null;

            return new ServiceEntity
            {
                Id = service.Id,
                Name = service.Name,
                Url = service.Url
            };
        }
    }
}
