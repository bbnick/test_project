using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MonitoringSystem.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using MonitoringSystem.Services;
using MonitoringSystem.Models;

namespace MonitoringSystem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            var cs = Configuration.GetSection("ConnectionStrings")["DefaultConnection"];

            services.AddDbContext<MSContext>(options => options
                .UseLazyLoadingProxies()
                .UseSqlServer(cs));

            services.AddHttpClient();

            services.AddScoped<IHttpService, HttpService>();
            services.AddScoped<IServiceRepository, ServiceRepository>();
            services.AddScoped<IServiceLogRepository, ServiceLogRepository>();

            services.AddHostedService<BgService>();

            services.Configure<BgServiceOptions>(Configuration.GetSection("BackgroundServiceOptions"));
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Monitor}/{action=Services}");
            });
        }
    }
}
