﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MonitoringSystem.DAL;
using MonitoringSystem.Extensions;
using MonitoringSystem.Models;
using MonitoringSystem.Services;

namespace MonitoringSystem.Controllers
{
    public class MonitorController : Controller
    {
        private readonly IServiceRepository _serviceRepository;
        private readonly IServiceLogRepository _serviceLogRepository;
        private readonly IHttpService _httpService;

        public MonitorController(IServiceRepository serviceRepository, IServiceLogRepository serviceLogRepository, IHttpService httpService)
        {
            _serviceLogRepository = serviceLogRepository;
            _serviceRepository = serviceRepository;
            _httpService = httpService;
        }

        public async Task<List<ServiceViewModel>> GetAllServices()
        {
            return await _serviceRepository.GetAll();
        }

        public async Task<string> GetServiceAvailability(string url)
        {
            var status = await _httpService.CheckService(url);
            return status.ToViewString();
        }

        public async Task<int> GetFailureServiceCount(Guid serviceId, DateTime dateFrom, DateTime dateTo)
        {
            var dateFilter = new DateFilterModel { DateFrom = dateFrom, DateTo = dateTo };
            return await _serviceLogRepository.StatusesCount(serviceId, Status.Unavailable, dateFilter);
        }

        public async Task<IActionResult> Services()
        {
            var services = await _serviceRepository.GetAll();
            var servicesData = new List<ServiceAvailibilityViewModel>();
            foreach(var service in services)
            {
                var status = await _httpService.CheckService(service.Url);

                var currentTime = DateTime.Now;
                var dateFilter = new DateFilterModel { DateTo = currentTime, DateFrom = currentTime.AddHours(-1) };
                var failureHourCount = await _serviceLogRepository.StatusesCount(service.Id, Status.Unavailable, dateFilter);

                dateFilter.DateTo = DateTime.Today;
                dateFilter.DateFrom = DateTime.Today.AddDays(-1);
                var failureDayCount = await _serviceLogRepository.StatusesCount(service.Id, Status.Unavailable, dateFilter);

                var serviceData = new ServiceAvailibilityViewModel
                {
                    Id = service.Id,
                    Name = service.Name,
                    Availability = status.ToViewString(),
                    FailureDayCount = failureDayCount,
                    FailureHourCount = failureHourCount
                };
                servicesData.Add(serviceData);
            }

            return View(servicesData);
        }

    }
}
