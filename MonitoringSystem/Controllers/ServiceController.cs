﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MonitoringSystem.DAL;
using MonitoringSystem.Extensions;
using MonitoringSystem.Models;
using MonitoringSystem.Services;

namespace MonitoringSystem.Controllers
{
    public class ServiceController : Controller
    {
        private readonly IServiceRepository _serviceRepository;

        public ServiceController(IServiceRepository serviceRepository)
        {
            _serviceRepository = serviceRepository;
        }

        public async Task<bool> AddService(string name, string url)
        {
            return await _serviceRepository.Set(new ServiceViewModel
            {
                Name = name,
                Url = url
            });
        }

        public async Task<List<ServiceViewModel>> GetAll()
        {
            return await _serviceRepository.GetAll();
        }

    }
}
