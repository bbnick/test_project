﻿using MonitoringSystem.DAL;
using MonitoringSystem.Models;
using MonitoringSystem.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MonitoringSystem.Tests
{
    public class ServiceLogRepositoryTests : MonitorTestBase
    {
        private readonly IServiceLogRepository _serviceLogRepository;

        public ServiceLogRepositoryTests()
        {
            _serviceLogRepository = new ServiceLogRepository(_context);
        }

        [Fact]
        public async Task StatusesCount_ShouldReturnOne_WhenLogsExists()
        {
            MSContextInitializer.Initialize(_context);

            var yandexService = MSContextInitializer.GetServiceEntities().FirstOrDefault(x => x.Name == "Yandex");
            var dateFilter = new DateFilterModel { DateFrom = DateTime.Now.AddDays(-1), DateTo = DateTime.Now.AddDays(1) };

            var result = await _serviceLogRepository.StatusesCount(yandexService.Id, Status.Unavailable, dateFilter);

            Assert.Equal(1, result);
        }

        [Fact]
        public async Task StatusesCount_ShoudRerturnZero_WhenLogsNotExists()
        {
            var yandexService = MSContextInitializer.GetServiceEntities().FirstOrDefault(x => x.Name == "Yandex");
            var dateFilter = new DateFilterModel { DateFrom = DateTime.Now.AddDays(-1), DateTo = DateTime.Now.AddDays(1) };

            var result = await _serviceLogRepository.StatusesCount(yandexService.Id, Status.Unavailable, dateFilter);

            Assert.Equal(0, result);
        }

        [Fact]
        public async Task StatusesCount_ShouldReturnTwo_WhenDateFilterIsNull()
        {
            MSContextInitializer.Initialize(_context);

            var yandexService = MSContextInitializer.GetServiceEntities().FirstOrDefault(x => x.Name == "Yandex");
            DateFilterModel dateFilter = null;

            var result = await _serviceLogRepository.StatusesCount(yandexService.Id, Status.Unavailable, dateFilter);

            Assert.Equal(2, result);
        }

        [Fact]
        public async Task StatusesCount_ShouldReturnZero_WhenServiceNotFound()
        {
            var dateFilter = new DateFilterModel { DateFrom = DateTime.Now.AddDays(-1), DateTo = DateTime.Now.AddDays(1) };

            var result = await _serviceLogRepository.StatusesCount(Guid.NewGuid(), Status.Unavailable, dateFilter);

            Assert.Equal(0, result);
        }
    }
}
