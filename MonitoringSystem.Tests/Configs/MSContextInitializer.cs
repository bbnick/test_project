﻿using MonitoringSystem.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonitoringSystem.Tests
{
    public class MSContextInitializer
    {
        public static void Initialize(MSContext context)
        {
            var services = GetServiceEntities();
            var serviceLogs = GetServiceLogEntities();

            context.Services.AddRange(services);
            context.ServiceLogs.AddRange(serviceLogs);

            context.SaveChanges();
        }

        public static List<ServiceEntity> GetServiceEntities()
        {
            return new List<ServiceEntity>()
            {
                new ServiceEntity
                {
                    Id = Guid.Parse("9DC06811-BE2F-4920-99C9-CFDFFFBBA213"),
                    Name = "Yandex",
                    Url = "https://yandex.ru"
                },
                new ServiceEntity
                {
                    Id = Guid.Parse("0E86080D-84AC-4945-BBAA-F6C2F7027DA6"),
                    Name = "Google",
                    Url = "https://www.google.com"
                }
            };
        }

        public static List<ServiceLogEntity> GetServiceLogEntities()
        {
            var services = GetServiceEntities();
            var yandexService = services.FirstOrDefault(x => x.Name == "Yandex");
            var googleService = services.FirstOrDefault(x => x.Name == "Google");

            return new List<ServiceLogEntity>()
            {
                new ServiceLogEntity
                {
                    Status = Status.Available,
                    SurveyTime = DateTime.Now,
                    ServiceId = yandexService.Id
                },
                new ServiceLogEntity
                {
                    Status = Status.Unavailable,
                    SurveyTime = DateTime.Now,
                    ServiceId = yandexService.Id
                },
                new ServiceLogEntity
                {
                    Status = Status.Unavailable,
                    SurveyTime = DateTime.Now.AddDays(-10),
                    ServiceId = yandexService.Id
                },
                new ServiceLogEntity
                {
                    Status = Status.Available,
                    SurveyTime = DateTime.Now,
                    ServiceId = googleService.Id
                },
                new ServiceLogEntity
                {
                    Status = Status.Unavailable,
                    SurveyTime = DateTime.Now,
                    ServiceId = googleService.Id
                }
            };
        }
    }
}
