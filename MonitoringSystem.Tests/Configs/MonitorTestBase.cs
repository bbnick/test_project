﻿using Microsoft.EntityFrameworkCore;
using MonitoringSystem.DAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonitoringSystem.Tests
{
    public class MonitorTestBase : IDisposable
    {
        protected readonly MSContext _context;

        public MonitorTestBase()
        {
            var options = new DbContextOptionsBuilder<MSContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            _context = new MSContext(options);

            _context.Database.EnsureCreated();
        }
        public void Dispose()
        {
            _context.Database.EnsureDeleted();

            _context.Dispose();
        }
    }
}
