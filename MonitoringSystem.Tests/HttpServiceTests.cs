﻿using Microsoft.Extensions.DependencyInjection;
using MonitoringSystem.DAL;
using MonitoringSystem.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MonitoringSystem.Tests
{
    public class HttpServiceTests
    {
        private readonly IHttpService _httpService;

        public HttpServiceTests()
        {
            IServiceCollection services = new ServiceCollection();
            services.AddHttpClient();
            var httpFactory = services
                .BuildServiceProvider()
                .GetRequiredService<IHttpClientFactory>();

            _httpService = new HttpService(httpFactory);
        }

        [Fact]
        public async Task CheckService_ShouldReturnAvailable_WhenUrlIsGoogle()
        {
            var url = "https://www.google.com";

            var result = await _httpService.CheckService(url);

            Assert.Equal(Status.Available, result);
        }

        [Fact]
        public async Task CheckService_ShouldReturnUnavailable_WhenUrlIsWrong()
        {
            var url = "";

            var result = await _httpService.CheckService(url);

            Assert.Equal(Status.Unavailable, result);
        }
    }
}
