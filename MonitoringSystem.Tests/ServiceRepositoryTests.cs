using Microsoft.EntityFrameworkCore;
using MonitoringSystem.DAL;
using MonitoringSystem.Extensions;
using MonitoringSystem.Models;
using MonitoringSystem.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace MonitoringSystem.Tests
{
    public class ServiceRepositoryTests : MonitorTestBase
    {
        private readonly IServiceRepository _serviceRepository;

        public ServiceRepositoryTests()
        {
            _serviceRepository = new ServiceRepository(_context);
        }

        [Fact]
        public async Task Get_ShouldReturnService_WhenServiceExists()
        {
            MSContextInitializer.Initialize(_context);
            var mockService = MSContextInitializer.GetServiceEntities().First();

            var result = await _serviceRepository.Get(mockService.Id);

            Assert.Equal(mockService.ToViewModel().Name, result.Name);
        }

        [Fact]
        public async Task Get_ShouldReturnNull_WhenServiceNotExists()
        {
            var result = await _serviceRepository.Get(Guid.NewGuid());

            Assert.Null(result);
        }

        [Fact]
        public async Task GetAll_ShouldReturnServices_WhenServicesExists()
        {
            MSContextInitializer.Initialize(_context);

            var result = await _serviceRepository.GetAll();

            var objectResult = Assert.IsType<List<ServiceViewModel>>(result);
            var services = Assert.IsAssignableFrom<List<ServiceViewModel>>(objectResult);
            Assert.Equal(2, services.Count());
        }

        [Fact]
        public async Task GetAll_ShouldReturnEmptyList_WhenServicesNotExists()
        {
            var result = await _serviceRepository.GetAll();

            var objectResult = Assert.IsType<List<ServiceViewModel>>(result);
            var services = Assert.IsAssignableFrom<List<ServiceViewModel>>(objectResult);
            Assert.Empty(services);
        }
    }
}
